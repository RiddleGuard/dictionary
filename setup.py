from setuptools import setup, find_packages

setup(name='Widjet_dictionary',
      version='0.1',
      description='Widjet the dictionary.',
      long_description='Widjet the dictionary.',
      classifiers=['Development Status :: 3 - Alpha',
                   'License :: OSI Approved :: MIT License',
                   'Programming Language :: Python :: 3.4.2',
                   'Topic :: Text Processing :: Linguistic',
                   ],
      keywords='english dictionary',
      author='Viktor Kondrahov',
      license='MIT',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False)
