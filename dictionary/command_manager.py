import sys
""" patern Command """
from abc import ABC, abstractmethod
from training_manager import TrainingManager
from dictionary import Dictionary
from card import Card

CSV_FILE = "dictionary.csv"
CASH_LERNING_WORDS="lerning.csv"

class AbstractCommand(ABC):
  """docstring for AbstractCommand"""
  def __init__(self, arg):
    super(AbstractCommand, self).__init__()
    self.arg = arg
    self.dictionary = Dictionary(CSV_FILE)
    self.manager = TrainingManager(self.dictionary)
    self.card = Card() 

  def saveBackup(self):
      pass

  def undo(self):
      pass

  @abstractmethod
  def execute(self):
    pass


class CommandHistory(object):
  """docstring for CommandHistory"""
  def __init__(self):
    super(CommandHistory, self).__init__()

  def push(self, cmd):
    pass

  def pop(self):
    pass


class CommandMng(object):
  history = CommandHistory()
  """docstring for CommandMng"""
  def __init__(self):
    super(CommandMng, self).__init__()

  def executeCommand(self, str_cmd):
      arg = str_cmd.split(" ")
      arg1, arg2 = arg[0], arg[1:]
      cmd = NoneCMD(arg2)
      if arg1 == "q": cmd = Exit(arg2)
      elif arg1 == "lern": cmd = Lern(arg2)
      elif arg1 == "add-word": cmd = AddWord(arg2)
      elif arg1 == "get-word": cmd = GetWord(arg2)
      elif arg1: cmd = Notfound(arg2)
      try:
        if cmd.execute(): self.history.push(cmd)
      except Exception as error:
        print('Warning: {}'.format(error))

  def undo(self):
      command = self.history.pop()
      if not command:
          command.undo()


class Exit(AbstractCommand):
  """docstring for Exit"""

  def execute(self):
    print("Exit")
    exit()


class NoneCMD(AbstractCommand):

  def execute(self):
      pass


class Notfound(AbstractCommand):

  def execute(self):
      print("Not found")


class Lern(AbstractCommand):
  """docstring for Lern"""
  def execute(self):
    print("Lern")

class AddWord(AbstractCommand):
  """docstring for Lern"""
  def execute(self):
      (word, translation) = self.arg
      self.dictionary.add_word(word, translation)

class GetWord(AbstractCommand):
  """docstring for GetWord"""
  def execute(self):
      self.card.print(self.dictionary, self.arg[0])

