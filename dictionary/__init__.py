# This Python file uses the following encoding: utf-8
import os
from training_manager import TrainingManager
from dictionary import Dictionary
from command_manager import *

CSV_FILE = "dictionary.csv"
CASH_LERNING_WORDS="lerning.csv"

def main():
    dicti = Dictionary(CSV_FILE)
    manager = TrainingManager(dicti)

    dicti.add_word("read", "читать")
    dicti.add_word("read", "читать")
    word_obj = dicti.get_word("читать")
    print(dicti.check_word(word_obj))

    print("")
    dicti.dictionary_info()
    manager.learning_info()

    command_mng = CommandMng()
    print("$> ")
    while True:
        applicant = input("$> ").split('\n')
        for line in applicant:
            command_mng.executeCommand(applicant)

main()