import sys
import csv
from datetime import datetime
from learning_object import LearningObject

# record in Dictionary
class StructDict(object):
    """docstring for StructDict"""
    def __init__(self, sd_word, sd_translation,
                 sd_timestamp=''):
        super(StructDict, self).__init__()
        self.sd_word = sd_word
        self.sd_translation = sd_translation
        if sd_timestamp == '':
            self.sd_timestamp = self.get_time()
        else:
            self.sd_timestamp = sd_timestamp

    @staticmethod
    def get_time():
        time_s = datetime.today()
        return time_s

# CRUD
class Dictionary(LearningObject):
    def __init__(self,  dict_file):
        super(Dictionary, self).__init__()
        self.dict_file = dict_file

    def get(self, arg):
        st = self.get_word(arg)
        return st.sd_word + ', ' + st.sd_translation



    def add_word(self, d_word, d_translation, d_timestamp=''):
        (_, res) = self.check_word(d_word)
        if res:
            print("The word is in the dictionary")
        else:
            word = StructDict(d_word, d_translation, d_timestamp)
            self.write_file_csv(word)
            print("The word added in the dictionary")


    @classmethod
    def delete_word(cls):
        pass

    @classmethod
    def update_word(cls):
        pass

    def get_word(self, word):
        tiem = datetime.today()
        csvrow = self.get_word_csv_file(word)
        if csvrow:
            return StructDict(*csvrow)
        return False

    def get_word_csv_file(self, word):
        with open(self.dict_file, 'r') as csvfile:
            reader = csv.reader(csvfile)
            try:
                for row in reader:
                    if (row[0] == word or row[1] == word):
                        return row
            except csv.Error as error:
                sys.exit('file {}, line {}: {}'.format(self.dict_file,
                                                       reader.line_num,
                                                       error))
        return False

    def get_words(self):
        arr = ""
        try:
            with open(self.dict_file) as file_handler:
                for line in file_handler:
                    arr += line
        except IOError:
            print("An IOError has occurred!")
        return arr


    def check_word(self, word):
        if isinstance(word, StructDict):
            return self.check_word(word.sd_word)
        with open(self.dict_file, 'r') as csvfile:
            reader = csv.reader(csvfile)
            try:
                for index, row  in enumerate(reader):
                    if (row[0] == word or row[1] == word):
                        return (index, True)
            except csv.Error as error:
                sys.exit('file {}, line {}: {}'.format(self.dict_file,
                                                       reader.line_num,
                                                       error))
        return (0, False)

    def write_file_csv(self, word):
        with open(self.dict_file, 'a') as csvfile:
            spamwriter = csv.writer(csvfile)
            spamwriter.writerow([word.sd_word, word.sd_translation,
                                 word.sd_timestamp])

    def count_of_words(self, chunk_size=1<<13):
        with open(self.dict_file) as file:
            return sum(chunk.count('\n')
                       for chunk in iter(lambda: file.read(chunk_size), ''))

    def dictionary_info(self):
        print("Count of words: {}".format(self.count_of_words()))
