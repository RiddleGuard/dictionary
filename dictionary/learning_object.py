from abc import ABC, abstractmethod

class LearningObject(ABC):
	"""docstring for LearningObject"""
	def __init__(self):
		super(LearningObject, self).__init__()
	
	@abstractmethod
	def get(self, args):
		pass
