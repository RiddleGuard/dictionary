import sys
from datetime import datetime, timedelta
from dictionary import StructDict

class TrainingManager(object):
    """docstring for TrainingManager"""
    short_shema = [0, 20, 480, 1440]

    full_shema = [0, 120, 1440, 10080, 40320]

    def __init__(self,
                 dictionary,
                 dict_name="none",
                 number=15,
                 mode="full"):
        super(TrainingManager, self).__init__()
        self.dict_name = dict_name
        self.number_of_words_studied = number
        self.dictionary = dictionary
        if mode == "short" or mode == "full":
            self.mode = mode
        else:
            sys.exit('Error: Unknow mdoe')

    def learning_words(self):
        self.dictionary

    def get_words_later_time(self, time):
        words = self.dictionary.get_words()
        for word in words.split("\n"):
            if word:
                s_word = StructDict(*word.split(","))
                date = datetime.strptime(s_word.sd_timestamp,
                                         '%Y-%m-%d %H:%M:%S.%f')
                target_date = timedelta(minutes=time)
                if datetime.today() - date < target_date:
                    print(word)


    def reiteration(self):
        pass

    def learning_info(self):
        print("Dictionary name: {dict_name}"
              "\nMode: {mode}"
              "\nNumber of words studied: {number}"
              .format(dict_name=self.dict_name,
                      mode=self.mode,
                      number=self.number_of_words_studied))
