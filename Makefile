COMPILER=python3
COMPILER_OPT=-u
APP=dictionary
TARGET=__init__.py
APP_TARGET=$(APP)/$(TARGET)

all: build

build:
	@$(COMPILER) $(COMPILER_OPT) $(APP_TARGET)

.PHONY: all
